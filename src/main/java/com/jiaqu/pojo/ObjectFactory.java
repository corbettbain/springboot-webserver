
package com.jiaqu.pojo;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.jiaqu.pojo package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.jiaqu.pojo
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link BankContractResponse }
     * 
     */
    public BankContractResponse createBankContractResponse() {
        return new BankContractResponse();
    }

    /**
     * Create an instance of {@link BankContract }
     * 
     */
    public BankContract createBankContract() {
        return new BankContract();
    }

    /**
     * Create an instance of {@link Country }
     * 
     */
    public Country createCountry() {
        return new Country();
    }

}
