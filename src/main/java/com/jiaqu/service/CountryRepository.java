package com.jiaqu.service;

import com.jiaqu.pojo.Country;

/**
 * Created by IntelliJ IDEA.
 * User: corbettbain
 * Date: 11/1 0001
 * Time: 15:07
 */
public interface CountryRepository {

    public Country findCountry(String name);
}
